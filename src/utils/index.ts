export function parseJwt(token: string) {
  if (!token) {
    return;
  }
  const base64Url = token.split(".")[1];
  const base64 = base64Url.replace("-", "+").replace("_", "/");
  return JSON.parse(window.atob(base64));
}

export const authToken = () => {
  const userStr = localStorage.getItem("user");
  let user = null;
  if (userStr) user = JSON.parse(userStr);
  if (user && user.accessToken) {
    return "Bearer " + user.accessToken;
  } else {
    return "";
  }
};

export const getCarImage = (car: string): string => {
  if (car.endsWith("Clio")) {
    return "./content/renault-clio.jpg";
  } else if (car.endsWith("Cayenne")) {
    return "./content/cayenne.jpg"
  } else if (car.endsWith("Aygo")) {
    return "./content/toyota-aygo.jpg"
  } else if (car.endsWith("Polestar 2")) {
    return "./content/Polestar_2.jpg"
  } else {
    return "./content/D-logo.jpg";
  }
};
