export type Car = {
    _id: string;
    name: string;
    available: boolean;
}

export type User = {
    sub: string;
    iat: number;
    exp: number;
}