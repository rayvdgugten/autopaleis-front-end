import React, { PropsWithChildren, useContext, useEffect } from "react";
import { User } from "../types";

type IAuth = {
  authenticated?: boolean;
  setAuthenticated: (authenticated: boolean) => void;
};

const AuthContext = React.createContext<IAuth>({
  authenticated: true,
  setAuthenticated: () => {},
});

export const AuthProvider: React.FC<PropsWithChildren> = ({ children }) => {
  const [authenticated, setAuthenticated] = React.useState(false);

  useEffect(() => {
    const userStr = localStorage.getItem("user");
    if (!userStr) {
      setAuthenticated(false);
    } else {
      const user: User = JSON.parse(userStr);
      if (Date.now() > user.exp * 1000) {
        setAuthenticated(false);
      } else {
        setAuthenticated(true);
      }
    }
    console.log(authenticated);
  }, [setAuthenticated, authenticated]);

  return (
    <AuthContext.Provider
      value={{ authenticated, setAuthenticated }}
    >
      {children}
    </AuthContext.Provider>
  );
};

export function useAuth() {
  return useContext(AuthContext);
}
