import { axiosInstance } from ".";
import { Car } from "../types";

export const getAllCars = async () => {
  return await axiosInstance.get("/api/car");
};

export const postCar = (car: Car) => {
  return axiosInstance.post("/api/car", car);
};

export const putCar = (car: Car) => {
  return axiosInstance.put(`/api/car/${car._id}`, car);
};
