import axios from "axios";
import { authToken } from "../utils";
import {getAllCars, postCar, putCar} from "./CarService";

export const axiosInstance = axios.create({
  baseURL: "http://localhost:8080",
  headers: {
    "Content-type": "application/json",
    "Access-Control-Allow-Origin": "*",
    "Authorization": authToken()
  },
});

export {getAllCars, postCar, putCar}
