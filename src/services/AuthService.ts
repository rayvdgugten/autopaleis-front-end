import axios from "axios";
import { User } from "../types";

const axiosInstance = axios.create({
    baseURL: "http://localhost:8080/api/auth",
    headers: {
      "Content-type": "application/json",
      "Access-Control-Allow-Origin": "*",
    },
  });

export const login = (username: string, password: string) => {
    return axiosInstance.post("/signin", { username, password })
    .then((response) => {
      if (response.data.accessToken) {
        localStorage.setItem("user", JSON.stringify(response.data));
      }
      return response.data;
    });
}

export const logout = () => {
  localStorage.removeItem("user");
};

export const getCurrentUser = () => {
  const userStr = localStorage.getItem("user");
  if (userStr) return JSON.parse(userStr);
  return null;
};

export const userIsAuthenticated = () => {
  let userStr = localStorage.getItem('user')
  if (!userStr) {
    return false
  }
  let user: User = JSON.parse(userStr)
  // if user has token expired, logout user
  if (Date.now() > user.exp * 1000) {
    logout()
    return false
  }
  return true
}