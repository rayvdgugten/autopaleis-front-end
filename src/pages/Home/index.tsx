import { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { Card } from "../../components";
import { getAllCars } from "../../services";
import { Car } from "../../types";
import "./Home.scss";

export const Home = () => {
  const [cars, setCars] = useState<Car[]>();

  useEffect(() => {
    getAllCars().then((response) => {
      setCars(response.data);
    }).catch((error) => {
      toast.error(error.message, {
        position: "top-center",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        });
    });
  }, []);

  return (
    <div className="Home">
      <div className="CarContainer">
        <div className="CarList">
          {cars
            ? cars
                .sort((a, b) => (a.available ? -1 : 1))
                .map((car) => (
                  <Card
                    key={car._id}
                    label={car.name}
                    available={car.available}
                  />
                ))
            : null}
        </div>
      </div>
    </div>
  );
};
