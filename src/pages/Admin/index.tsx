import { useEffect, useState } from "react";
import { toast } from "react-toastify";
import { CarList, Controls, Toast } from "../../components";
import { getAllCars, postCar, putCar } from "../../services";
import { Car } from "../../types";
import "./Admin.scss";

export const Admin = () => {
  const [cars, setCars] = useState<Car[]>();
  const [selectedId, setSelectedId] = useState<string>("");
  const [selectedName, setSelectedName] = useState<string>("");
  const [selectedAvailability, setSelectedAvailability] =
    useState<boolean>(false);

  const handleCreateCar = (car: Car) => {
    postCar(car).then((response) => {
      if (response.status === 200) {
        getAllCars().then((response) => {
          setCars(response.data);
        });
        toast.success('Car added succesfully!', {
          position: "top-center",
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          });
      }
    }).catch((error) => {
      toast.error(error.message, {
        position: "top-center",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        });
    });
  };

  const handleUpdateCar = (car: Car) => {
    putCar(car).then((response) => {
      if (response.status === 200) {
        getAllCars().then((response) => {
          setCars(response.data);
        });
        toast.success('Car updated succesfully!', {
          position: "top-center",
          autoClose: 2000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
          });
      }
    }).catch((error) => {
      toast.error(error.message, {
        position: "top-center",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
        });
    });
  };

  const handleSelect = (name: string, availability: boolean, _id: string) => {
    setSelectedName(name);
    setSelectedAvailability(availability);
    setSelectedId(_id);
  };

  useEffect(() => {
    getAllCars().then((response) => {
      setCars(response.data);
    });
  }, []);

  return (
    <div className="Admin">
      <CarList cars={cars} onSelect={handleSelect} />
      <Controls
        selectedName={selectedName}
        selectedAvailability={selectedAvailability}
        selectedId={selectedId}
        onCreateCar={handleCreateCar}
        onUpdateCar={handleUpdateCar}
        setSelectedName={setSelectedName}
        setSelectedAvailability={setSelectedAvailability}
      />
      <Toast />
    </div>
  );
};
