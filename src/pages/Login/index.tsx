import { useState } from "react";
import { useNavigate } from "react-router-dom";
import { Button, TextInput } from "../../components";
import { login } from "../../services/AuthService";
import LoginIcon from "@mui/icons-material/Login";
import "./Login.scss";
import { toast } from "react-toastify";

type LoginProps = {
  authenticated: boolean;
  setAuthenticated: (isAuth: boolean) => void;
};

export const Login = ({ authenticated, setAuthenticated }: LoginProps) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [validationError, setValidatedError] = useState(false);
  const navigate = useNavigate();

  const handleLogin = () => {
    setValidatedError(false);
    if (username === "" || password === "") {
      setValidatedError(true);
      toast.error('Username or password invalid', {
        position: "top-center",
        autoClose: 2000,
        hideProgressBar: false,
        closeOnClick: true,
        pauseOnHover: true,
        draggable: true,
        progress: undefined,
      });
    } else {
      login(username, password)
        .then(() => {
          setAuthenticated(true);
          if (authenticated) {
            toast.success("Authenticate succesfull!", {
              position: "top-center",
              autoClose: 2000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
            navigate("/admin", { replace: true });
          }
        })
        .catch((error) => {
          toast.error(error.message, {
            position: "top-center",
            autoClose: 2000,
            hideProgressBar: false,
            closeOnClick: true,
            pauseOnHover: true,
            draggable: true,
            progress: undefined,
          });
        });
    }
  };
  return (
    <div className="LoginScreen">
      <div className="Login">
        <div className="Container">
          <div className="LogoWrapper">
            <img
              className="Logo"
              src={require("./content/admin.png")}
              alt="Admin"
            />
          </div>
          <div className="FieldWrapper">
            <TextInput
              className={validationError ? "validationError" : ""}
              placeholder={"Username"}
              value={username}
              onChange={(e) => setUsername(e.target.value)}
            />
            <TextInput
              className={validationError ? "validationError" : ""}
              password
              placeholder={"●●●●●●"}
              value={password}
              onChange={(e) => setPassword(e.target.value)}
            />
          </div>
          <div className="ButtonWrapper">
            <Button
              onClick={handleLogin}
              label={"Login"}
              icon={<LoginIcon />}
            />
          </div>
        </div>
      </div>
    </div>
  );
};
