import React, { useCallback, useContext, useEffect, useState } from "react";
import { User } from "../../types";

type AuthProps = {
  children: React.ReactNode
}

const AuthContext = React.createContext({});

export const AuthProvider = ({ children }: AuthProps) => {
  const [user, setUser] = useState<User | null>();

  const getUser = useCallback((): User | null => {
    const userString = localStorage.getItem("user");

    if(!userString) {
      return null;
    }
    return JSON.parse(userString).data;
  }, []);

  useEffect(() => {
    setUser(getUser());
  }, [setUser, getUser]);

  const userIsAuthenticated = () => {
    let strUser = localStorage.getItem("user");
    if (!strUser) {
      return false;
    }
    const dataObject = JSON.parse(strUser);
    const user: User = dataObject.data;
    // if user has token expired, logout user
    if (Date.now() > user.exp * 1000) {
      userLogout();
      return false;
    }

    return true;
  };

  const userLogin = (user: User) => {
    localStorage.setItem("user", JSON.stringify(user));
    setUser(user);
  };

  const userLogout = () => {
    localStorage.removeItem("user");
    setUser(null);
  };

  return (
    <AuthContext.Provider value={{user, getUser, userIsAuthenticated, userLogin, userLogout}}>
      {children}
    </AuthContext.Provider>
  )
};

export const useAuth = () => {
  return useContext(AuthContext);
}
