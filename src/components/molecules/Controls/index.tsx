import { Car } from "../../../types";
import "./Controls.scss";

type ControlsProps = {
  selectedName: string;
  selectedAvailability: boolean;
  selectedId: string;
  onCreateCar: (car: Car) => void;
  onUpdateCar: (car: Car) => void;
  setSelectedAvailability: (availability: boolean) => void;
  setSelectedName: (name: string) => void;
};

export const Controls = ({ selectedName, selectedAvailability, selectedId, onCreateCar, onUpdateCar, setSelectedName, setSelectedAvailability }: ControlsProps) => {
  return (
    <div className="tableControls">
      <div className="inputContainer">
        <input
          className="name"
          type="text"
          placeholder="Name"
          value={selectedName}
          onChange={e => setSelectedName(e.target.value)}
        />
        <input
          className="model"
          type="checkbox"
          placeholder="Model"
          checked={selectedAvailability}
          onChange={e => setSelectedAvailability(e.target.checked)}
        />
      </div>
      <div className="buttonContainer">
        <button
          className="create"
          onClick={() => onCreateCar({_id: "", name: selectedName, available: selectedAvailability})}
        >
          Create
        </button>
        <button className="update" onClick={() => onUpdateCar({_id: selectedId, name: selectedName, available: selectedAvailability})}>Update</button>
        <button className="delete">Delete</button>
      </div>
    </div>
  );
};
