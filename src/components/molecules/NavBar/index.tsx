import { Link, useNavigate } from "react-router-dom";
import { Button } from "../../atoms";
import "./NavBar.scss";

type NavProps = {
  authenticated: boolean;
  onLogOut: () => void;
};

export const NavBar = ({ authenticated, onLogOut }: NavProps) => {
  const navigate = useNavigate();
  const handleLogOut = () => {
    onLogOut();
    navigate('/');
  };
  return (
    <div className="NavBar">
      <Link className="Link" to={"/"}><img className="Logo" src={require('../../../content/D-logo.jpg')} alt="HOME"/></Link>
      {authenticated ? (
        <div className="AdminNav">
          <Link className="Link" to={"/admin"}>
            Admin
          </Link>
          <Button onClick={handleLogOut} label={"Log out"} />
        </div>
      ) : null}
    </div>
  );
};
