import { Car } from "../../../types";
import { CarItem, Counter } from "../../atoms";
import "./CarList.scss";

type CarListProps = {
  cars?: Car[];
  onSelect: (name: string, availability: boolean, _id: string) => void;
};

export const CarList = ({ cars, onSelect }: CarListProps) => {
  const totalAvailable = cars?.filter((car) => car.available).length;
  return (
    <div>
      <div className="counterContainer">
        <Counter title="Available" count={totalAvailable ? totalAvailable : 0}/>
        <Counter title="Total" count={cars ? cars.length : 0}/>
      </div>
      <div className="carContainer">
        {cars
          ? cars
              .sort((a, b) => a.name.localeCompare(b.name))
              .sort((a, b) => (a.available ? -1 : 1))
              .map((car: Car) => (
                <CarItem key={car._id} car={car} onSelect={onSelect}/>
              ))
          : null}
      </div>
    </div>
  );
};
