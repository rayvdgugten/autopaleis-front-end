import { Car } from "../../../types";
import "./CarItem.scss";

type CarItemProps = {
  onSelect: (name: string, availability: boolean, _id: string) => void;
  car: Car;
};

export const CarItem = ({ onSelect, car }: CarItemProps) => {
  return (
    <div className="carItemWrapper">
      <div
        className={car.available ? "available" : ""}
        onClick={() => onSelect(car.name, car.available, car._id)}
      >
        <label>{car.name}</label>
      </div>
    </div>
  );
};
