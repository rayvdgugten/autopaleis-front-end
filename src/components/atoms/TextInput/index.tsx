import "./TextInput.scss";
import PasswordIcon from '@mui/icons-material/Password';
import AccountCircleIcon from '@mui/icons-material/AccountCircle';

type TextInputProps = {
  placeholder: string;
  onChange: (e: any) => void;
  value: string;
  password?: boolean;
  className?: string;
};

export const TextInput = ({
  placeholder,
  value,
  password,
  onChange,
  className,
}: TextInputProps) => {
  return (
    <div className={`InputWrapper ${className}`}>
      {password ? <PasswordIcon className="Icon"/> : <AccountCircleIcon className="Icon" />}
      <input
        className="Input"
        type={password ? "password" : "text"}
        value={value}
        onChange={onChange}
        placeholder={placeholder}
      />
    </div>
  );
};
