import { getCarImage } from "../../../utils";
import "./Card.scss";
type CardProps = {
  label: string;
  available: boolean;
};

export const Card = ({ label, available }: CardProps) => {
  
  return (
    <div className="CardContainer">
      <div className={`${available ? 'Available' : ''} : Card`}>
        <div className="Overlay">
          <div className="ImageWrapper">
            <img
              className="Image"
              src={require("" + getCarImage(label))}
              alt={label}
            />
          </div>
          <div className="InfoWrapper">
            <div className="Label">{label}</div>
          </div>
        </div>
      </div>
    </div>
  );
};
