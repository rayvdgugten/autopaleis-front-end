import "./Counter.scss";

type CounterProps = {
    title: string;
    count: number;
}

export const Counter = ({title, count}: CounterProps) => {
    return (
        <div className="counterWrapper">
          <div>{title}</div>
          <div>{count}</div>
        </div>
    )
}