import { render, screen } from '@testing-library/react';
import { Button } from '../index';
test("renders learn react link", () => {
  render(<Button label='TestLabel' onClick={() => {}} />);
  const linkElement = screen.getByText(/TestLabel/i);
  expect(linkElement).toBeInTheDocument();
});
