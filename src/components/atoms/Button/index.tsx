import "./Button.scss";

type ButtonProps = {
  label: string;
  onClick: () => void;
  icon?: React.ReactNode;
};

export const Button = ({ label, icon, onClick }: ButtonProps) => {
  return (
    <div className="Button" onClick={onClick}>
      {icon ? (
        <div className="iconWrapper">{icon ? icon : null}</div>
      ) : (
        <div className="Label">{label}</div>
      )}
    </div>
  );
};
