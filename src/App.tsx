import React, { useEffect } from "react";
import { Routes, Route, Navigate } from "react-router-dom";
import "./App.scss";
import { NavBar, Toast } from "./components";
import { Admin, Home, Login } from "./pages";
import { logout, userIsAuthenticated } from "./services/AuthService";

function App() {
  const [authenticated, setAuthenticated] = React.useState(false);

  useEffect(() => {
    setAuthenticated(userIsAuthenticated());
  }, [setAuthenticated, authenticated]);
  return (
    <div className="App">
      <NavBar
        authenticated={authenticated}
        onLogOut={() => {
          logout();
          setAuthenticated(false);
        }}
      />
      <Routes>
        <Route path={"/"} element={<Home />} />
        <Route
          path={"/login"}
          element={<Login setAuthenticated={setAuthenticated} authenticated />}
        />
        {authenticated ? (
          <Route path={"/admin"} element={<Admin />} />
        ) : (
          <Route path="*" element={<Navigate to={"/login"} />} />
        )}
      </Routes>
      <Toast />
    </div>
  );
}

export default App;
